# duckieBot

## Instructions
1. Install pipenv and dependencies
In MAC OS:
```
pip3 install pipenv
pipenv install
```
2. Create a .env with the following variables:

```
ACCESS_TOKEN=<your access token in quotes>
VERIFY_TOKEN=<verify token in quotes>
```

Obviously, you still need to replace the `<...>`.

## Collaborators

* Diego Cánez
* Diego Linares
