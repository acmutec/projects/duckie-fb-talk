import random
import os
from flask import Flask, request
from pymessenger.bot import Bot
from dotenv import load_dotenv
from pathlib import Path

load_dotenv('../')
app = Flask(__name__)
bot = Bot(os.getenv("ACCESS_TOKEN"))

@app.route('/', methods=['GET'])
def handle_verification():
    print("path: ", Path('.').resolve())
    print("test verify_token: ", os.getenv("VERIFY_TOKEN"))
    if (request.args.get('hub.verify_token', '') == os.getenv("VERIFY_TOKEN")):
        print("Verified")
        return request.args.get('hub.challenge', '')
    else:
        print("Wrong token")
        return "Error, wrong validation token"

@app.route('/', methods=['POST'])
def webhook():
    output = request.get_json()
    for event in output['entry']:
        messaging = event['messaging']
        for message in messaging:
            if message.get('message'):
                recipient_id = message['sender']['id']
                message_text = message['message'].get('text')
                if message_text:
                    if message_text[0] == '$':
                        # Process command query
                        response_text = get_command(message_text[1:])
                    else:
                        # Process NLP query
                        response_text = get_answer(message_text)

                    send_message(recipient_id, response_text)

    return ("Message Processed")


def get_answer(text):
    return (
        "Model hasn't been trained yet. Is a 'Hello' good enough for an answer?"
    )


def get_command(text):
    tokens = text.split()
    command = tokens[0]
    args = tokens[1:]

    # if(command == "execute"):
    return ("Commands are not yet supported.")


def send_message(recipient_id, response):
    bot.send_text_message(recipient_id, response)
    return ("success")


if __name__ == '__main__':
    app.run()
